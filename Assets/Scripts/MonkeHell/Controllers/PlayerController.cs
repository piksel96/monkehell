using MonkeHell.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    private GameObject[] multipleEnemys;
    public Transform closestEnemy;
    public bool enemyContact;


    // Start is called before the first frame update
    void Start()
    {
        closestEnemy = null;
        enemyContact = false;

    }

    // Update is called once per frame
    void Update()
    {
        //transform.LookAt(getClosestEnemy().position);


        if (Input.GetKey("r"))
        {
            ScoreManager.Instance.Reload();
        }


    }


    /*public Transform getClosestEnemy()
    {
        multipleEnemys = GameObject.FindGameObjectsWithTag("Enemys");
        float closestDistance = Mathf.Infinity;
        Transform trans = null;

        foreach (GameObject go in multipleEnemys)
        {
            float currentDistance;
            currentDistance = Vector3.Distance(transform.position, go.transform.position);
            if (currentDistance < closestDistance)
            {
                closestDistance = currentDistance;
                trans = go.transform;
            }
        }
        return trans;

    }*/


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Bullet"))
        {
            Destroy(collision.gameObject,1);
            ScoreManager.Instance.DecreaseHp();

        }
    }


   
}