using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{


    public GameObject bullet;

    public int hp;

    private GameObject monke;

    float countDown = 0;

    public Slider slider;
    public Transform sliderTransform;

    // Start is called before the first frame update
    void Start()
    {
        slider.value = 100;
        
        
    }

    // Update is called once per frame
    void Update()
    {
        monke = GameObject.Find("monke");
        float distance = Vector3.Distance(monke.transform.position, transform.position);

        slider.transform.rotation = Quaternion.identity;

        transform.LookAt(monke.transform.position);

        if (distance > 4)
        {
            transform.position = Vector3.MoveTowards(transform.position, monke.transform.position, 0.005f);
        }

        if (distance < 3)
        {
            transform.position = Vector3.MoveTowards(transform.position, monke.transform.position, -0.005f);
        }
        


        countDown += Time.deltaTime;

        

        if( countDown >= 1)
        {
            int randomNumb = Random.Range(1, 600);

            if (randomNumb == 1)
            {
                Transform bulletPos = this.gameObject.transform.GetChild(3).gameObject.transform;
                GameObject newBul = Instantiate(bullet, bulletPos.position, bulletPos.rotation);

                newBul.GetComponent<Rigidbody>().velocity = newBul.transform.up * -3f;


                countDown = 0;
                Destroy(newBul, 5);
            }

        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Bullet"))
        {
            print(collision);
            Destroy(collision.gameObject);
            hp -=50;
            slider.value =hp;
            if (hp <= 0) {
                Destroy(this.gameObject);
            }
            
        }
    }

    void Awake()
    {
        sliderTransform = slider.transform;
    }
    void LateUpdate()
    {
        
    }


}
