using MonkeHell.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MonkeHell.Controllers
{
    public class CameraController : MonoBehaviour
    {
        public Vector3 offset;
        private GameObject _currentPlayer;
        public bool isCameraReadyToFollow;
        public bool isCameraReadToPlay;
        private void Start()
        {

        }

        private void FixedUpdate()
        {
            if (_currentPlayer != null )
            {
                FollowPlayer(_currentPlayer);
            }

        }

        public void FollowPlayer(GameObject currentPlayer)
        {
            if (_currentPlayer == null)
            {
                _currentPlayer = currentPlayer;
            }
            gameObject.transform.position = Vector3.Lerp(transform.position, new Vector3(_currentPlayer.transform.GetChild(0).position.x,4,-5),0.05f);
            gameObject.transform.LookAt(currentPlayer.transform.position + offset);
        }
    }
}