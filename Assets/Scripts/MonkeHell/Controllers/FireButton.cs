using MonkeHell.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireButton : MonoBehaviour
{

    public GameObject bullet;
    private GameObject monke;
    private GameObject ak47;

    private GameObject realBullet;
    private Transform realBulletPos;

    public AudioSource fireSound;

    public float countDown = 0;





    void Start()
    {

        

        monke = GameObject.Find("monke");
        ak47 = GameObject.Find("Ak_47");
        realBullet = monke.gameObject.transform.GetChild(4).gameObject;

        
        ak47.GetComponent<Animator>().enabled = false;



    }

    void Update()
    {
        
        

        realBulletPos = realBullet.transform;

        

        if (ak47.GetComponent<Animator>().enabled)
        {
            countDown += Time.deltaTime;

            if (countDown >= 0.5f)
            {
                ak47.GetComponent<Animator>().enabled = false;
                countDown = 0;
            }
        }
    }

    public void Fire()
    {

        if (ScoreManager.Instance.instanceBullet > 0)
        {
            GameObject newBul = Instantiate(bullet, realBulletPos.position, realBulletPos.rotation);
            newBul.GetComponent<Rigidbody>().velocity = -realBulletPos.transform.up * 40f;
            Destroy(newBul, 5);

            AudioSource newSound = Instantiate(fireSound, realBulletPos.position, realBulletPos.rotation);
            Destroy(newSound, 1);

            ScoreManager.Instance.DecreaseBullet();

            ak47.GetComponent<Animator>().enabled = true;
            //ak47.GetComponent<Animation>().Play("bayonet");

        }

        else
        {

        }

        

    }


}
