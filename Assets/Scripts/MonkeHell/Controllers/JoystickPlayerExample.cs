﻿using MonkeHell.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickPlayerExample : MonoBehaviour
{
    public float speed;
    private VariableJoystick variableJoystick;
    public Rigidbody rb;


    private void Update()
    {
        
    }

    private void Start()
    {
        variableJoystick = UIManager.Instance.joystick2.gameObject.GetComponent<VariableJoystick>();
    }
    public void FixedUpdate()
    {
        Vector3 direction = Vector3.forward * variableJoystick.Vertical + Vector3.right * variableJoystick.Horizontal;
        rb.velocity=(direction * speed * Time.fixedDeltaTime);
    }
}