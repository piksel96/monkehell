using MonkeHell.Controllers;
using MonkeHell.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using TosbyGames.Core.UI;
using UnityEngine;

namespace MonkeHell.UI
{
    public class StartButton : UIButton
    {
        private CameraController _cameraController;
        protected override void DoAction()
        {
            StartGame();
            Invoke("StartCamera", 1.5f);
        }

        private void StartCamera()
        {
            _cameraController.isCameraReadToPlay = true;
            _cameraController.isCameraReadyToFollow = true;
            RUIPanel.Open("GamePlay");
            UIManager.Instance.joystick2.gameObject.SetActive(true);
        }

        private void StartGame()
        {
            _cameraController = FindObjectOfType<CameraController>();
            LevelManager.Instance.isGamePlayable = true;
            gameObject.SetActive(false);

        }
    }
}