using MonkeHell.Controllers;
using System.Collections;
using System.Collections.Generic;
using TosbyGames.Core.Patterns.Creational;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MonkeHell.Managers
{
    public class LevelManager : Singleton<LevelManager>
    {
        public LevelHelper currentLevelHelper;
        public CameraController cameraController;
        public bool isGamePlayable;
        public bool isGameFinished = false;
        private int _levelIndex;
        // Start is called before the first frame update
        void Start()
        {
            cameraController = Camera.main.GetComponent<CameraController>();
            _levelIndex = PlayerPrefs.GetInt("LevelIndex", 1);
            CreateLevelHelper();
        }

        private void CreateLevelHelper()
        {
            LevelHelper cloneLevel;
            if (_levelIndex <= LevelHelperRepository.LevelHelperCount())
            {
                cloneLevel = Instantiate(LevelHelperRepository.GetLevel(_levelIndex));
                //Elephant.LevelStarted(_levelIndex);
                //Debug.Log("LEVEL STARTED // " + _levelIndex);
            }
            else
            {
                //cloneLevel = Instantiate(LevelHelperRepository.GetRandomLevel());
                _levelIndex = 1;
                cloneLevel = Instantiate(LevelHelperRepository.GetLevel(_levelIndex));

            }
            currentLevelHelper = cloneLevel;
            cameraController.FollowPlayer(currentLevelHelper.currentPlayer);
        }
        public void GetNextLevel()
        {
            //Elephant.LevelCompleted(_levelIndex);
            _levelIndex++;
            PlayerPrefs.SetInt("LevelIndex", _levelIndex);
            //UIManager.Instance.InceraseLevelNumberText();
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        public void RestartLevel()
        {
            //Elephant.LevelFailed(_levelIndex);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}