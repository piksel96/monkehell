using System.Collections;
using System.Collections.Generic;
using TosbyGames.Core.Patterns.Creational;
using UnityEngine;

namespace MonkeHell.Managers
{

    public class UIManager : Singleton<UIManager>
    {

        public GameObject joystick1;
        public GameObject joystick2;

        private int _levelNumber = 1;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        public void InceraseLevelNumberText()
        {
            _levelNumber++;
            PlayerPrefs.SetInt("LevelNumber", _levelNumber);
        }
    }

}