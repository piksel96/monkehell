using System.Collections;
using System.Collections.Generic;
using TosbyGames.Core.Patterns.Creational;
using UnityEngine;
using UnityEngine.UI;

namespace MonkeHell.Managers
{
   
    public class ScoreManager : Singleton<ScoreManager>
    {

        public Text hpText ;
        public Text bulletText;
        public Text Score;


        public int instanceBullet = 30;
        public int maxBullet = 60;
        public int hp = 100;

        // Start is called before the first frame update
        void Start()
        {
            hpText.text = "100";
            bulletText.text = "30 / 60";
            Score.text = "0";
        }

        // Update is called once per frame
        void Update()
        {

            hpText.text = hp.ToString();
            bulletText.text = instanceBullet.ToString() + " / " + maxBullet.ToString();

        }


        public void DecreaseBullet()
        {
            instanceBullet--;
        }

        public void Reload()
        {
            maxBullet += instanceBullet;
            instanceBullet = 0;

            for (int i = 0; i < 30; i++)
            {
                if (maxBullet == 0)
                {
                    break;
                }

                instanceBullet++;
                maxBullet--;

            }

        }

        public void DecreaseHp()
        {
            hp -= 10;
            if (hp <= 0)
            {
                Time.timeScale = 0;
            }
        }
    }
}
