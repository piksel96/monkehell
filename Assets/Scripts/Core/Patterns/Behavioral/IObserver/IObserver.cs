﻿using TosbyGames.Core.Patterns.Behavioral.IObserver;

namespace TosbyGames.Core.Patterns.Behavioral
{
    public interface IObserver<T>
    {
        void Update(IObservable<T> observable);
    }
}