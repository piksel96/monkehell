﻿using System;

namespace TosbyGames.Core.UI
{
    public interface IPresentable
    {
        event Action OnUpdatedEvent;
    }
}