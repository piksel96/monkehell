using System.Collections;
using System.Collections.Generic;
using TosbyGames.Core.Patterns.Creational;
using UnityEngine;

namespace MonkeHell
{
    public class LevelHelper : Singleton<LevelHelper>
    {
        public int levelIndex;
        public GameObject currentPlayer;
    }
}
