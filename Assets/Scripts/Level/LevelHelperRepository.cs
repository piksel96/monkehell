using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TosbyGames.Core.UnityObjectRepository;
using TosbyGames.Core.Utility;

namespace MonkeHell
{
    public static class LevelHelperRepository
    {
        private static readonly UnityObjectRepository<LevelHelper> levelHelperRepository =
                                                    new UnityObjectRepository<LevelHelper>("LEVELS");

        public static LevelHelper GetLevel(int level)
        {
            return levelHelperRepository.Items.SingleOrDefault(e => e.levelIndex == level);
        }

        public static LevelHelper GetRandomLevel() => levelHelperRepository.Items.GetRandomItem();

        public static int LevelHelperCount()
        {
            return levelHelperRepository.Items.Count;
        }
    }
}